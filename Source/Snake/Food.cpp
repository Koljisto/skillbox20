// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeActor.h"
#include "Engine/World.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::AddFood(int Quantity)
{
	for (int i = 0; i < Quantity; i++)
	{
		int32 elemX = FMath::RandRange(1, 18);
		int32 elemY = FMath::RandRange(1, 18);
		int32 foodtype = FMath::RandRange(1, 3);
		//int32 foodtype = 1;
		FVector NewLocation(elemX * 150, elemY * 150, 0);
		FRotator NewRotator(0, 0, 0);
		FVector NewScale3D(0.45, 0.45, 0.45);
		FTransform NewTransform(NewRotator, NewLocation, NewScale3D);
		AFood* food = GetWorld()->SpawnActor<AFood>(FoodElemClass, NewTransform);
		if (foodtype == 1)
		{
			food->TypeFoodElem = TypeFood::One;
		}
		else if (foodtype == 2)
		{
			food->TypeFoodElem = TypeFood::SpeedUp;
		}
		else if (foodtype == 3)
		{
			food->TypeFoodElem = TypeFood::SpeedDown;
		}
	}
}

//void AFood::FoodElementOverlap(AFood * OverlappedElement, AActor * Other)
//{
//	if (IsValid(OverlappedElement)) {
//		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
//		if (InteractableInterface) {
//			InteractableInterface->Interact(this, false);
//		}
//	}
//}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeActor>(Interactor);
		if (IsValid(Snake)) {
			if (this->TypeFoodElem == TypeFood::SpeedUp) {
				UE_LOG(LogTemp, Warning, TEXT("UP %f"), Snake->MovementSpeed * 0.9);
				Snake->SetMovementSpeed(Snake->MovementSpeed * 0.9);
			}
			if (this->TypeFoodElem == TypeFood::SpeedDown) {
				UE_LOG(LogTemp, Warning, TEXT("DOWN %f"), Snake->MovementSpeed * 1.1);
				Snake->SetMovementSpeed(Snake->MovementSpeed * 1.1);
			}
			Snake->AddSnakeElement();
			this->AddFood();
			this->Destroy();
		}
	}
}

