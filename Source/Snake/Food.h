// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "FoodType.h"
#include "Food.generated.h"
//17x17 -- 95 x 95 ->Start; 100 x 100 size

UENUM()
enum class TypeFood {
	One,
	SpeedUp,
	SpeedDown
};

UCLASS()
class SNAKE_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodElemClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TypeFood TypeFoodElem;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddFood(int Quantity = 1);

	//UFUNCTION()
	//	void FoodElementOverlap(AFood* OverlappedElement, AActor* Other);

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
