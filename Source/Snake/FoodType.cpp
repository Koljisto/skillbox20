// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodType.h"
#include "Food.h"

// Sets default values
AFoodType::AFoodType()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

}

// Called when the game starts or when spawned
void AFoodType::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodType::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

