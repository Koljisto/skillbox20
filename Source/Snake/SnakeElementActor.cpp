// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementActor.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeActor.h"
#include "Food.h"

// Sets default values
ASnakeElementActor::ASnakeElementActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementActor::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementActor::HandleBeginOverlap);
}

void ASnakeElementActor::Interact(AActor * Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeActor>(Interactor);
	if (IsValid(Snake)) {
		Snake->Destroy();
	}
}

void ASnakeElementActor::HandleBeginOverlap(
	UPrimitiveComponent * OverlappedComponent, 
	AActor * OtherActor, 
	UPrimitiveComponent * OtherComp,
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult &SweepResult) 
{
	if (IsValid(SnakeOwner)) {
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementActor::DisableActor(bool bHide)
{
	if (bHide == true) {
		SetActorHiddenInGame(false);
		SetActorEnableCollision(true);
		SetActorTickEnabled(true);
	} else {
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		SetActorTickEnabled(false);
	}
}

void ASnakeElementActor::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision) {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	} else {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

